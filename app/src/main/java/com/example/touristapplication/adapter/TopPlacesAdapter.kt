package com.example.touristapplication.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.touristapplication.ui.DetailsActivity
import com.example.touristapplication.model.Place
import com.example.touristapplication.R
import com.example.touristapplication.ui.utils.Util.NAME
import java.util.*

class TopPlacesAdapter(context: Context, topPlacesDataList: List<Place>) :
    RecyclerView.Adapter<TopPlacesAdapter.TopPlacesViewHolder?>(), Filterable {

    var context: Context
    var topPlacesDataList: List<Place>
    var placesListFiltered: ArrayList<Place> = ArrayList()

    init {
        this.context = context
        this.topPlacesDataList = topPlacesDataList
        placesListFiltered.addAll(topPlacesDataList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopPlacesViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.top_places_row_item, parent, false)
        return TopPlacesViewHolder(view)
    }

    override fun onBindViewHolder(holder: TopPlacesViewHolder, position: Int) {
        holder.countryName.text = placesListFiltered[position].country
        holder.placeName.text = placesListFiltered[position].cityName
        holder.placeImage.setImageResource(placesListFiltered[position].imageUrl)

        holder.itemView.setOnClickListener {
            val i = Intent(context, DetailsActivity::class.java)
            placesListFiltered[position].lastTimeClicked = Calendar.getInstance().timeInMillis
            i.putExtra(NAME, placesListFiltered[position])
            context.startActivity(i)
        }
    }

    override fun getItemCount(): Int {
        return placesListFiltered.size
    }

    class TopPlacesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var placeImage: ImageView
        var placeName: TextView
        var countryName: TextView


        init {
            placeImage = itemView.findViewById(R.id.place_image)
            placeName = itemView.findViewById(R.id.city_name)
            countryName = itemView.findViewById(R.id.country_name)
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val listFilter = ArrayList<Place>();
                if (p0 == null || p0.isEmpty()) {
                    listFilter.addAll(topPlacesDataList)
                } else {
                    val filterPattern: String = p0.toString().lowercase(Locale.getDefault()).trim()
                    for (item in topPlacesDataList) {
                        if (item.cityName.lowercase(Locale.getDefault())
                                .contains(filterPattern) or item.cityName.lowercase(Locale.getDefault())
                                .contains(filterPattern) or item.country.contains(filterPattern)
                            or item.country.lowercase(Locale.getDefault()).contains(filterPattern)
                        ) {
                            listFilter.add(item)
                        }
                    }
                }
                val results = FilterResults()
                results.values = listFilter
                return results;
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {

                placesListFiltered.clear()
                placesListFiltered.addAll(p1?.values as Collection<Place>)

                notifyDataSetChanged()
            }
        }
    }
}