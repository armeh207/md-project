package com.example.touristapplication.model

data class User(val firstName: String? = null,
                val lastName: String? = null,
                val email:String? = null,
                val password: String? = null){

}

