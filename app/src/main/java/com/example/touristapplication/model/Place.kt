package com.example.touristapplication.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.LocalDate
import java.io.Serializable
import java.util.Calendar

@Entity(tableName = "placesTable")
data class Place(
    @PrimaryKey
    val placeId: Int,
    var cityName: String,
    var country: String,
    val imageUrl: Int,
    val aboutText: String,
    var isSaved: Boolean = false,
    var lastTimeClicked: Long = 0
) : Serializable