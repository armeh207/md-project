package com.example.touristapplication.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.media.Image
import android.os.Bundle
import android.webkit.CookieManager
import android.webkit.WebView
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.touristapplication.R


class HotelActivity : AppCompatActivity() {

    lateinit var progressBar: ProgressBar

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel)
        progressBar = findViewById(R.id.progressBar)
        val webView = findViewById<WebView>(R.id.webView)
        val homeButton = findViewById<ImageView>(R.id.imageView3)
        val profileButton = findViewById<ImageView>(R.id.imageView4)
        val flightButton = findViewById<ImageView>(R.id.imageView6)
        homeButton.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        profileButton.setOnClickListener{
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }
        flightButton.setOnClickListener{
            val intent = Intent(this, FlightActivity::class.java)
            startActivity(intent)
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, false);
        } else {
            CookieManager.getInstance().setAcceptCookie(false);
        }
        val url = "https://www.booking.com/"
        webView.webViewClient = WebViewClient()
        webView.loadUrl(url)
        webView.settings.allowContentAccess = true
        webView.settings.javaScriptEnabled = true


    }

    inner class WebViewClient : android.webkit.WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return false
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            progressBar.isVisible = false
        }
    }

}
//fun Fragment.hotelActivity(): HotelActivity? = activity as HotelActivity