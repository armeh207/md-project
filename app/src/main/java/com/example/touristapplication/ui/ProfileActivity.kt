package com.example.touristapplication.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.touristapplication.R
import com.example.touristapplication.adapter.TopPlacesAdapter
import com.example.touristapplication.db.AppDatabase
import com.example.touristapplication.model.Place

class ProfileActivity : AppCompatActivity() {
    private val appDatabase by lazy { AppDatabase.getDatabase(this).placeDAO() }
    private lateinit var savedList: List<Place>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        savedList = getSavedPlaces()
        setSavedRecycler(savedList)
        val homeButton = findViewById<ImageView>(R.id.imageView3)
        val flightButton = findViewById<ImageView>(R.id.imageView6)
        val hotelButton = findViewById<ImageView>(R.id.imageView5)
        homeButton.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        flightButton.setOnClickListener{
            val intent = Intent(this, FlightActivity::class.java)
            startActivity(intent)
        }
        hotelButton.setOnClickListener{
            val intent = Intent(this, HotelActivity::class.java)
            startActivity(intent)
        }
    }
    private fun setSavedRecycler(savedList:List<Place>){
        val savedPlacesRecycler = findViewById<RecyclerView>(R.id.savedPlaces)
        savedPlacesRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val adapter = TopPlacesAdapter(this, savedList)
        savedPlacesRecycler.adapter = adapter
    }
    private fun getSavedPlaces():List<Place>{
        return appDatabase.getSavedPlaces()
    }

    override fun onResume() {
        super.onResume()
        savedList = getSavedPlaces()
        setSavedRecycler(savedList)
    }
}