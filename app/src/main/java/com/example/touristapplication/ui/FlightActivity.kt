package com.example.touristapplication.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.webkit.CookieManager
import android.webkit.WebView
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.touristapplication.R

class FlightActivity : AppCompatActivity(){

    lateinit var progressBar: ProgressBar

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flight)
        progressBar = findViewById(R.id.progressBar)
        val webView = findViewById<WebView>(R.id.webViewFlights)
        val homeButton = findViewById<ImageView>(R.id.imageView3)
        val profileButton = findViewById<ImageView>(R.id.imageView4)
        val hotelButton = findViewById<ImageView>(R.id.imageView5)
        homeButton.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        profileButton.setOnClickListener{
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }
        hotelButton.setOnClickListener{
            val intent = Intent(this, HotelActivity::class.java)
            startActivity(intent)
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, false);
        } else {
            CookieManager.getInstance().setAcceptCookie(false);
        }
        val url = "https://www.skyscanner.ie/"
        webView.webViewClient = WebViewClient()
        webView.loadUrl(url)
        webView.settings.allowContentAccess = true
        webView.settings.javaScriptEnabled = true

    }

    inner class WebViewClient : android.webkit.WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return false
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            progressBar.isVisible = false
        }
    }
}
