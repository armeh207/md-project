package com.example.touristapplication.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.touristapplication.R
import com.example.touristapplication.adapter.RecentAdapter
import com.example.touristapplication.adapter.TopPlacesAdapter
import com.example.touristapplication.db.AppDatabase
import com.example.touristapplication.model.Place

class MainActivity : AppCompatActivity() {

    private val appDatabase by lazy { AppDatabase.getDatabase(this).placeDAO() }
    lateinit var hotelImage: ImageView
    lateinit var flightImage: ImageView
    lateinit var profileImage: ImageView
    lateinit var recentAdapter: RecentAdapter
    lateinit var recentTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        hotelImage = findViewById(R.id.imageView5)
        hotelImage.setOnClickListener {
            openHotels()
        }
        flightImage = findViewById(R.id.imageView6)
        flightImage.setOnClickListener {
            openFlights()
        }
        profileImage = findViewById(R.id.imageView4)
        profileImage.setOnClickListener {
            openProfile()
        }

        val topPlacesList = getListOfPlaces()
        val recentDataList =
            topPlacesList.filter { it.lastTimeClicked != 0L }.sortedBy { it.lastTimeClicked }
                .reversed()
        recentTextView = findViewById(R.id.recentTextView)

        recentTextView.isVisible = recentDataList.isNotEmpty()
        setRecentsRecycler(recentDataList)

        setTopPlacesRecycler(topPlacesList)
    }

    private fun setRecentsRecycler(recentsDataList: List<Place>) {
        val recentRecycler: RecyclerView = findViewById<RecyclerView>(R.id.recent_recycler)
        recentRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recentAdapter = RecentAdapter(this, recentsDataList)
        recentRecycler.adapter = recentAdapter
    }

    private fun setTopPlacesRecycler(TopPlacesDataList: List<Place>) {
        val topPlacesRecycler: RecyclerView = findViewById(R.id.top_places_recycler)
        topPlacesRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val adapter = TopPlacesAdapter(this, TopPlacesDataList)
        topPlacesRecycler.adapter = adapter
        val searchView = findViewById<androidx.appcompat.widget.SearchView>(R.id.searchView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                recentAdapter.filter.filter(newText)
                return false
            }
        })
    }

    private fun openHotels() {
        val intent = Intent(this, HotelActivity::class.java)
        startActivity(intent)
    }

    private fun openFlights() {
        val intent = Intent(this, FlightActivity::class.java)
        startActivity(intent)
    }

    private fun openProfile() {
        val intent = Intent(this, ProfileActivity::class.java)
        startActivity(intent)
    }

    private fun getListOfPlaces(): List<Place> {
        return appDatabase.getAllPlaces().ifEmpty {
            val places = arrayListOf<Place>()
            places.add(
                Place(
                    1,
                    "Dublin",
                    "Ireland",
                    R.drawable.dublin,
                    "Dublin is the capital and largest city of Ireland. On a bay at the mouth of the River Liffey, it is in the province of Leinster, bordered on the south by the Dublin Mountains, a part of the Wicklow Mountains range. At the 2016 census it had a population of 1,173,179, while the preliminary results of the 2022 census recorded that County Dublin as a whole had a population of 1,450,701, and that the population of the Greater Dublin Area was over 2 million, or roughly 40% of the Republic of Ireland's total population." +
                            "\n" +
                            "A settlement was established in the area by the Gaels during or before the 7th century, followed by the Vikings. As the Kingdom of Dublin grew, it became Ireland's principal settlement by the 12th century Anglo-Norman invasion of Ireland. The city expanded rapidly from the 17th century and was briefly the second largest in the British Empire and sixth largest in Western Europe after the Acts of Union in 1800. Following independence in 1922, Dublin became the capital of the Irish Free State, renamed Ireland in 1937."
                )
            )
            places.add(
                Place(
                    2,
                    "Galway",
                    "Ireland",
                    R.drawable.galway,
                    "Galway is a city in the West of Ireland, in the province of Connacht, which is the county town of County Galway. It lies on the River Corrib between Lough Corrib and Galway Bay, and is the sixth most populous city on the island of Ireland and the fourth most populous in the Republic of Ireland, with a population at the 2022 census of 83,456." +
                            "\n" +
                            "Located near an earlier settlement, Galway grew around a fortification built by the King of Connacht in 1124. A municipal charter in 1484 allowed citizens of the by then walled city to form a council and mayoralty. Controlled largely by a group of merchant families, the Tribes of Galway, the city grew into a trading port. Following a period of decline, as of the 21st century, Galway is a tourist destination known for festivals and events including the Galway Arts Festival."
                )
            )
            places.add(
                Place(
                    3,
                    "Cork",
                    "Ireland",
                    R.drawable.cork,
                    "Cork is the second largest city in Ireland and third largest city by population on the island of Ireland. It is located in the south-west of Ireland, in the province of Munster. Following an extension to the city's boundary in 2019, its population is over 222,000." +
                            "\n" +
                            "The city centre is an island positioned between two channels of the River Lee which meet downstream at the eastern end of the city centre, where the quays and docks along the river lead outwards towards Lough Mahon and Cork Harbour, one of the largest natural harbours in the world."
                )
            )
            places.add(
                Place(
                    4,
                    "Barcelona",
                    "Spain",
                    R.drawable.barcelona,
                    "Barcelona is a city on the coast of northeastern Spain. It is the capital and largest city of the autonomous community of Catalonia, as well as the second most populous municipality of Spain. With a population of 1.6 million within city limits, its urban area extends to numerous neighbouring municipalities within the Province of Barcelona and is home to around 4.8 million people, making it the fifth most populous urban area in the European Union after Paris, the Ruhr area, Madrid, and Milan. It is one of the largest metropolises on the Mediterranean Sea, located on the coast between the mouths of the rivers Llobregat and Besòs, and bounded to the west by the Serra de Collserola mountain range, the tallest peak of which is 512 metres (1,680 feet) high."
                )
            )
            places.add(
                Place(
                    5,
                    "London",
                    "United Kingdom",
                    R.drawable.london,
                    "London is the capital and largest city of England and the United Kingdom, with a population of just under 9 million. It stands on the River Thames in south-east England at the head of a 50-mile (80 km) estuary down to the North Sea, and has been a major settlement for two millennia. The City of London, its ancient core and financial centre, was founded by the Romans as Londinium and retains its medieval boundaries. The City of Westminster, to the west of the City of London, has for centuries hosted the national government and parliament. Since the 19th century,[11] the name \"London\" has also referred to the metropolis around this core, historically split between the counties of Middlesex, Essex, Surrey, Kent, and Hertfordshire,[12] which largely comprises Greater London, governed by the Greater London Authority." +
                            "\n" +
                            "As one of the world's major global cities, London exerts a strong influence on its arts, entertainment, fashion, commerce and finance, education, health care, media, science and technology, tourism, and transport and communications."
                )
            )
            places.add(
                Place(
                    6,
                    "Madrid",
                    "Spain",
                    R.drawable.madrid,
                    "Madrid is the capital and most populous city of Spain. The city has almost 3.4 million inhabitants and a metropolitan area population of approximately 6.7 million. It is the second-largest city in the European Union (EU), and its monocentric metropolitan area is the third-largest in the EU. The municipality covers 604.3 km2 (233.3 sq mi) geographical area." +
                            "\n" +
                            "Madrid lies on the River Manzanares in the central part of the Iberian Peninsula. Capital city of both Spain (almost without interruption since 1561) and the surrounding autonomous community of Madrid (since 1983), it is also the political, economic and cultural centre of the country. The city is situated on an elevated plain about 300 km (190 mi) from the closest seaside location. The climate of Madrid features hot summers and cool winters."
                )
            )
            places.add(
                Place(
                    7,
                    "Paris",
                    "France",
                    R.drawable.paris,
                    "Paris is the capital and most populous city of France, with an estimated population of 2,165,423 residents in 2019 in an area of more than 105 km² (41 sq mi), making it the 30th most densely populated city in the world in 2020. Since the 17th century, Paris has been one of the world's major centres of finance, diplomacy, commerce, fashion, gastronomy, and science. For its leading role in the arts and sciences, as well as its very early system of street lighting, in the 19th century it became known as \"the City of Light\". Like London, prior to the Second World War, it was also sometimes called the capital of the world." +
                            "\n" +
                            "The City of Paris is the centre of the Île-de-France region, or Paris Region, with an estimated population of 12,262,544 in 2019, or about 19% of the population of France, making the region France's primate city. The Paris Region had a GDP of €739 billion (\$743 billion) in 2019, which is the highest in Europe. According to the Economist Intelligence Unit Worldwide Cost of Living Survey, in 2022, Paris was the city with the ninth-highest cost of living in the world."
                )
            )
            places.add(
                Place(
                    8,
                    "Rome",
                    "Italy",
                    R.drawable.rome,
                    "Rome is the capital city of Italy. It is also the capital of the Lazio region, the centre of the Metropolitan City of Rome, and a special comune named Comune di Roma Capitale. With 2,860,009 residents in 1,285 km2 (496.1 sq mi), Rome is the country's most populated comune and the third most populous city in the European Union by population within city limits. The Metropolitan City of Rome, with a population of 4,355,725 residents, is the most populous metropolitan city in Italy. Its metropolitan area is the third-most populous within Italy. Rome is located in the central-western portion of the Italian Peninsula, within Lazio (Latium), along the shores of the Tiber. Vatican City (the smallest country in the world) is an independent country inside the city boundaries of Rome, the only existing example of a country within a city. Rome is often referred to as the City of Seven Hills due to its geographic location, and also as the \"Eternal City\". Rome is generally considered to be the \"cradle of Western civilization and Christian culture\", and the centre of the Catholic Church." +
                            "\n" +
                            "Rome's history spans 28 centuries. While Roman mythology dates the founding of Rome at around 753 BC, the site has been inhabited for much longer, making it a major human settlement for almost three millennia and one of the oldest continuously occupied cities in Europe."
                )
            )
            places.add(
                Place(
                    9,
                    "Milan",
                    "Italy",
                    R.drawable.milan,
                    "Milan is a city in northern Italy, capital of Lombardy, and the second-most populous city proper in Italy after Rome. The city proper has a population of about 1.4 million, while its metropolitan city has 3.26 million inhabitants. Its continuously built-up urban area (whose outer suburbs extend well beyond the boundaries of the administrative metropolitan city and even stretch into the nearby country of Switzerland) is the fourth largest in the EU with 5.27 million inhabitants. According to national sources, the population within the wider Milan metropolitan area (also known as Greater Milan), is estimated between 8.2 million and 12.5 million making it by far the largest metropolitan area in Italy and one of the largest in the EU." +
                            "\n" +
                            "Milan is considered a leading alpha global city, with strengths in the fields of art, chemicals, commerce, design, education, entertainment, fashion, finance, healthcare, media (communication), services, research and tourism. Its business district hosts Italy's stock exchange, and the headquarters of national and international banks and companies."
                )
            )
            places.add(
                Place(
                    10,
                    "Berlin",
                    "Germany",
                    R.drawable.berlin,
                    "Berlin is the capital and largest city of Germany by both area and population. Its 3.6 million inhabitants make it the European Union's most populous city, according to population within city limits. One of Germany's sixteen constituent states, Berlin is surrounded by the State of Brandenburg and contiguous with Potsdam, Brandenburg's capital. Berlin's urban area, which has a population of around 4.5 million, is the second most populous urban area in Germany after the Ruhr. The Berlin-Brandenburg capital region has around 6.2 million inhabitants and is Germany's third-largest metropolitan region after the Rhine-Ruhr and Rhine-Main regions." +
                            "\n" +
                            "Berlin straddles the banks of the Spree, which flows into the Havel (a tributary of the Elbe) in the western borough of Spandau. Among the city's main topographical features are the many lakes in the western and southeastern boroughs formed by the Spree, Havel and Dahme, the largest of which is Lake Müggelsee. Due to its location in the European Plain, Berlin is influenced by a temperate seasonal climate. About one-third of the city's area is composed of forests, parks, gardens, rivers, canals, and lakes. The city lies in the Central German dialect area, the Berlin dialect being a variant of the Lusatian-New Marchian dialects."
                )
            )
            appDatabase.insertNewPlaces(places)
            places
        }
    }

    override fun onResume() {
        super.onResume()
        val topPlacesList = getListOfPlaces()
        val recentDataList =
            topPlacesList.filter { it.lastTimeClicked != 0L }.sortedBy { it.lastTimeClicked }
                .reversed()
        recentTextView.isVisible = recentDataList.isNotEmpty()
        setRecentsRecycler(recentDataList)

        setTopPlacesRecycler(topPlacesList)
    }

}