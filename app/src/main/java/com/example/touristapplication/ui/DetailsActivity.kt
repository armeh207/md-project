package com.example.touristapplication.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.touristapplication.model.Place
import com.example.touristapplication.R
import com.example.touristapplication.adapter.RecentAdapter
import com.example.touristapplication.db.AppDatabase
import com.example.touristapplication.ui.utils.Util.NAME


class DetailsActivity : AppCompatActivity() {

    private lateinit var recentsAdapter: RecentAdapter
    private val appDatabase by lazy { AppDatabase.getDatabase(this).placeDAO() }
    //private var viewHolder: RecentsAdapter.RecentsViewHolder = RecentsAdapter.RecentsViewHolder(View)
    private lateinit var place: Place
    private lateinit var aboutText:TextView
    private lateinit var bookButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        place = intent.getSerializableExtra(NAME) as Place
        appDatabase.insertPlace(place)
        initView()

    }
    private fun startSecondActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
    fun initView(){
        aboutText = findViewById(R.id.aboutText)
        bookButton = findViewById(R.id.bookButton)
        val location = findViewById<TextView>(R.id.location)
        location.text = place.cityName+", "+place.country
        val image1 = findViewById<ImageView>(R.id.imageView8)
        image1.setImageResource(place.imageUrl)
        aboutText.text = place.aboutText
        bookButton.setOnClickListener{
            val intent = Intent(this, FlightActivity::class.java)
            startActivity(intent)
        }
//        if(viewHolder.placeName.text=="Dublin") {
//            place.cityName = "Dublin"
//            place.country = "Ireland"
//            aboutText.text = "Dublin about text"
//        }
        val arrowBack:ImageView = findViewById<ImageView>(R.id.arrowBack)
        arrowBack.setOnClickListener { startSecondActivity() }
        val savedButton:ImageView = findViewById<ImageView>(R.id.saveButton)
        var isSaved = place.isSaved
        if (!isSaved) {
            savedButton.setImageResource(R.drawable.ic_baseline_bookmark_add_24)
        } else{
            savedButton.setImageResource(R.drawable.ic_baseline_bookmark_added_24)
        }
        savedButton.setOnClickListener {
            isSaved = if (!isSaved) {
                place.isSaved = true
                savedButton.setImageResource(R.drawable.ic_baseline_bookmark_added_24)
                Toast.makeText(applicationContext, "Place added to saved", Toast.LENGTH_SHORT).show()
                true
            } else{
                place.isSaved = false
                savedButton.setImageResource(R.drawable.ic_baseline_bookmark_add_24)
                Toast.makeText(applicationContext, "Place removed from saved", Toast.LENGTH_SHORT).show()
                false
            }
            appDatabase.insertPlace(place)
        }
    }

}




