package com.example.touristapplication.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.touristapplication.model.Place

@Dao
interface PlaceDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewPlaces(places:List<Place>)

    @Query("SELECT * FROM placesTable")
    fun getAllPlaces():List<Place>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPlace(place: Place)

    @Query ("SELECT * FROM placesTable WHERE isSaved=1")
    fun getSavedPlaces():List<Place>
}