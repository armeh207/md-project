package com.example.touristapplication.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.touristapplication.model.Place

@Database(entities = [Place::class], version = 1, exportSchema = true)
abstract class AppDatabase : RoomDatabase() {

    abstract fun placeDAO(): PlaceDAO

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = buildDatabase(context)
                }
            }
            return INSTANCE!!
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "places_database"
            ).allowMainThreadQueries().build()
        }
    }
}